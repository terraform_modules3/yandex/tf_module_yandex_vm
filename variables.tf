# https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance

variable "folder_id" {
  description = "ID of the folder where vm will be created (list folders: yc resource-manager folder list)"
  type        = string
}

variable "name" {
  description = "Name of virtual machine"
  type        = string
  default     = ""
}

variable "description" {
  description = "Description of virtual machine"
  type        = string
  default     = ""
}

variable "labels" {
  description = <<EOT
  key/value pairs to assign to vm
  Example:
  labels = {
    env = "dev"
    tier = "backend"
  }
  EOT
  type        = map(string)
  default     = {}
}

variable "platform_id" {
  description = "Type of virtual machine to create https://cloud.yandex.com/en-ru/docs/compute/concepts/vm-platforms"
  type        = string
  default     = "standard-v3"
}

variable "zone" {
  description = "Availability zone where virtual machine will be created https://cloud.yandex.com/en-ru/docs/overview/concepts/geo-scope"
  type        = string
  default     = "ru-central1-a"
}

variable "is_preemptible" {
  description = "VM that can be terminated by Yandex Compute Cloud at any time https://cloud.yandex.com/en-ru/docs/compute/concepts/preemptible-vm"
  type        = bool
  default     = false
}


variable "cpu_cores" {
  type    = number
  default = 2
}

variable "memory_gb" {
  description = "memory size in GB"
  type        = number
  default     = 2
}

variable "core_fraction" {
  description = "baseline performance for a core as a percent https://cloud.yandex.com/en-ru/docs/compute/concepts/performance-levels"
  type        = number
  default     = 100
}


variable "disk_size_gb" {
  description = "Size of the disk in GB"
  type        = number
  default     = 8
}

variable "disk_type" {
  description = "network-ssd | network-hdd | network-ssd-nonreplicated | network-ssd-io-m3 https://cloud.yandex.com/en-ru/docs/compute/concepts/disk"
  type        = string
  default     = "network-ssd"
}


variable "ssh_user_name" {
  description = "Name of a user that will be created on the virtual machin. Default: ubuntu"
  type        = string
  default     = ""
}

variable "ssh_user_public_key" {
  description = "Ssh public key. Must begin with 'ssh-rsa', 'ssh-ed25519', 'ecdsa-sha2-nistp256', 'ecdsa-sha2-nistp384', or 'ecdsa-sha2-nistp521'"
  type        = string
}


variable "subnet_id" {
  description = "ID of the subnet to attach vm interface to (list subnets: yc vpc subnet list)"
  type        = string
}

variable "private_ip_address" {
  description = "The private IP address to assign to the instance. If empty, the address will be automatically assigned from the specified subne"
  type        = string
  default     = null
}

variable "attach_public_ip" {
  description = "Provide a public ip address for virtual machine"
  type        = bool
  default     = false
}

variable "public_ip_address" {
  description = "Provide existing ip address"
  type        = string
  default     = ""
}


variable "image_name" {
  description = "The name of the image. Example: ubuntu-22-04-lts-v20230911. Only one of `image_name`, `image_id`, `image_family` can be provided"
  type        = string
  default     = ""
}

variable "image_id" {
  description = "The ID of a specific image. Only one of `image_name`, `image_id`, `image_family` can be provided"
  type        = string
  default     = ""
}

variable "image_family" {
  description = "The family name of an image. Used to search the latest image in a family. Example: ubuntu-2204-lts. Only one of `image_name`, `image_id`, `image_family` can be provided"
  type        = string
  default     = ""
}

variable "allow_stopping_for_update" {
  description = "Allows Terraform to stop the instance in order to update its properties."
  type        = bool
  default     = false
}

variable "manage_etc_hosts" {
  description = "Override /etc/hosts content on reboot"
  type        = bool
  default     = true
}

variable "enable_oslogin" {
  description = <<EOT
  Connect to over SSH with an SSH certificate via the YC CLI or a standard SSH client
  https://yandex.cloud/en/docs/organization/concepts/os-login
  EOT

  type    = bool
  default = null
}

variable "user_data" {
  description = <<EOF
  allows to add cloud init content to instance

  E.g.:

  user_data = <<EOT
  #cloud-config
  ---
  ssh_pwauth: false
  serial-port-enable: false
  manage_etc_hosts: false
  write_files:
    - content: |
        #!/usr/bin/env sh

        echo 123

      path: /var/lib/cloud/scripts/per-boot/bootstrap.sh
      permissions: "0755"
  EOT
  EOF

  type    = string
  default = null
}

variable "docker_compose" {
  description = "docker-compose content for instance based on Computer Optimezed Images"
  type        = string
  default     = null
}

variable "security_group_ids" {
  description = "Security group ids for network interface"
  type        = list(string)
  default     = null
}

variable "image_folder_id" {
  type    = string
  default = "standard-images"
}

variable "service_account_id" {
  description = "ID of the service account authorized for this instance."
  type        = string
  default     = null
}
