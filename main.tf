


resource "yandex_compute_instance" "vm" {
  name        = var.name
  description = var.description
  folder_id   = var.folder_id

  labels = var.labels

  platform_id = var.platform_id
  zone        = var.zone

  allow_stopping_for_update = var.allow_stopping_for_update

  service_account_id = var.service_account_id

  resources {
    cores         = var.cpu_cores
    memory        = var.memory_gb
    core_fraction = var.core_fraction
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot_disk.id
  }

  scheduling_policy {
    preemptible = var.is_preemptible
  }

  network_interface {
    subnet_id          = var.subnet_id
    ip_address         = var.private_ip_address
    nat                = var.attach_public_ip
    nat_ip_address     = var.public_ip_address
    security_group_ids = var.security_group_ids
  }

  metadata = {
    ssh-keys = var.ssh_user_name == "" ? "ubuntu:${var.ssh_user_public_key}" : null
    user-data = var.user_data != null ? var.user_data : templatefile("${path.module}/templates/cloud-init.tftpl", {
      ssh_user_name       = var.ssh_user_name,
      ssh_user_public_key = var.ssh_user_public_key,
      manage_etc_hosts    = var.manage_etc_hosts
      }
    )
    docker-compose = var.docker_compose
    enable-oslogin = var.enable_oslogin
  }

  lifecycle {
    ignore_changes = [boot_disk[0].initialize_params]
  }
}

# tflint-ignore: terraform_required_providers
data "yandex_compute_image" "vm_image" {
  family    = var.image_family
  name      = var.image_name
  image_id  = var.image_id
  folder_id = var.image_folder_id
}

resource "yandex_compute_disk" "boot_disk" {
  folder_id = var.folder_id
  zone      = var.zone
  image_id  = data.yandex_compute_image.vm_image.id
  size      = var.disk_size_gb
  type      = var.disk_type

  lifecycle {
    ignore_changes = [image_id]
  }
}
