# Yandex Cloud Virtual Machine Terraform Module
Simple module to create and manage virtual machine in Yandex cloud provider

## Usage example
```hcl
module "proxy_vm" {
  source = "git::https://gitlab.com/terraform_modules3/yandex/tf_module_yandex_vm.git?ref=0.1.0"

  name = "proxy"

  folder_id           = "b1…34"
  subnet_id           = "e9…us"
  ssh_user_public_key = "ssh-rsa AAAA…cRN"

  image_family = "ubuntu-2204-lts"

  attach_public_ip = true
}
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) | > 0.90 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_yandex"></a> [yandex](#provider\_yandex) | 0.122.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [yandex_compute_disk.boot_disk](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_disk) | resource |
| [yandex_compute_instance.vm](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance) | resource |
| [yandex_compute_image.vm_image](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/compute_image) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_stopping_for_update"></a> [allow\_stopping\_for\_update](#input\_allow\_stopping\_for\_update) | Allows Terraform to stop the instance in order to update its properties. | `bool` | `false` | no |
| <a name="input_attach_public_ip"></a> [attach\_public\_ip](#input\_attach\_public\_ip) | Provide a public ip address for virtual machine | `bool` | `false` | no |
| <a name="input_core_fraction"></a> [core\_fraction](#input\_core\_fraction) | baseline performance for a core as a percent https://cloud.yandex.com/en-ru/docs/compute/concepts/performance-levels | `number` | `100` | no |
| <a name="input_cpu_cores"></a> [cpu\_cores](#input\_cpu\_cores) | n/a | `number` | `2` | no |
| <a name="input_description"></a> [description](#input\_description) | Description of virtual machine | `string` | `""` | no |
| <a name="input_disk_size_gb"></a> [disk\_size\_gb](#input\_disk\_size\_gb) | Size of the disk in GB | `number` | `8` | no |
| <a name="input_disk_type"></a> [disk\_type](#input\_disk\_type) | network-ssd \| network-hdd \| network-ssd-nonreplicated \| network-ssd-io-m3 https://cloud.yandex.com/en-ru/docs/compute/concepts/disk | `string` | `"network-ssd"` | no |
| <a name="input_enable_oslogin"></a> [enable\_oslogin](#input\_enable\_oslogin) | Connect to over SSH with an SSH certificate via the YC CLI or a standard SSH client<br>  https://yandex.cloud/en/docs/organization/concepts/os-login | `bool` | `null` | no |
| <a name="input_folder_id"></a> [folder\_id](#input\_folder\_id) | ID of the folder where vm will be created (list folders: yc resource-manager folder list) | `string` | n/a | yes |
| <a name="input_image_family"></a> [image\_family](#input\_image\_family) | The family name of an image. Used to search the latest image in a family. Example: ubuntu-2204-lts. Only one of `image_name`, `image_id`, `image_family` can be provided | `string` | `""` | no |
| <a name="input_image_id"></a> [image\_id](#input\_image\_id) | The ID of a specific image. Only one of `image_name`, `image_id`, `image_family` can be provided | `string` | `""` | no |
| <a name="input_image_name"></a> [image\_name](#input\_image\_name) | The name of the image. Example: ubuntu-22-04-lts-v20230911. Only one of `image_name`, `image_id`, `image_family` can be provided | `string` | `""` | no |
| <a name="input_is_preemptible"></a> [is\_preemptible](#input\_is\_preemptible) | VM that can be terminated by Yandex Compute Cloud at any time https://cloud.yandex.com/en-ru/docs/compute/concepts/preemptible-vm | `bool` | `false` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | key/value pairs to assign to vm<br>  Example:<br>  labels = {<br>    env = "dev"<br>    tier = "backend"<br>  } | `map(string)` | `{}` | no |
| <a name="input_manage_etc_hosts"></a> [manage\_etc\_hosts](#input\_manage\_etc\_hosts) | Override /etc/hosts content on reboot | `bool` | `true` | no |
| <a name="input_memory_gb"></a> [memory\_gb](#input\_memory\_gb) | memory size in GB | `number` | `2` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of virtual machine | `string` | `""` | no |
| <a name="input_platform_id"></a> [platform\_id](#input\_platform\_id) | Type of virtual machine to create https://cloud.yandex.com/en-ru/docs/compute/concepts/vm-platforms | `string` | `"standard-v3"` | no |
| <a name="input_private_ip_address"></a> [private\_ip\_address](#input\_private\_ip\_address) | The private IP address to assign to the instance. If empty, the address will be automatically assigned from the specified subne | `string` | `null` | no |
| <a name="input_public_ip_address"></a> [public\_ip\_address](#input\_public\_ip\_address) | Provide existing ip address | `string` | `""` | no |
| <a name="input_ssh_user_name"></a> [ssh\_user\_name](#input\_ssh\_user\_name) | Name of a user that will be created on the virtual machin. Default: ubuntu | `string` | `""` | no |
| <a name="input_ssh_user_public_key"></a> [ssh\_user\_public\_key](#input\_ssh\_user\_public\_key) | Ssh public key. Must begin with 'ssh-rsa', 'ssh-ed25519', 'ecdsa-sha2-nistp256', 'ecdsa-sha2-nistp384', or 'ecdsa-sha2-nistp521' | `string` | n/a | yes |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | ID of the subnet to attach vm interface to (list subnets: yc vpc subnet list) | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | Availability zone where virtual machine will be created https://cloud.yandex.com/en-ru/docs/overview/concepts/geo-scope | `string` | `"ru-central1-a"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_vm_info"></a> [vm\_info](#output\_vm\_info) | virtual machine info and stats |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
